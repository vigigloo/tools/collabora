variable "namespace" {
  type = string
}

# tflint-ignore: terraform_unused_declarations
variable "edition" {
  type    = string
  default = "team"
}

variable "chart_name" {
  type = string
}

variable "chart_version" {
  type    = string
  default = "0.0.2"
}

variable "values" {
  type    = list(string)
  default = []
}

variable "image_repository" {
  type    = string
  default = "collabora/code"
}

variable "image_tag" {
  type    = string
  default = "21.11.2.4.1"
}

variable "helm_force_update" {
  type    = bool
  default = false
}

variable "helm_recreate_pods" {
  type    = bool
  default = false
}

variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}

variable "helm_max_history" {
  type    = number
  default = 0
}

variable "limits_cpu" {
  type    = string
  default = "1000m"
}

variable "limits_memory" {
  type    = string
  default = "1024Mi"
}

variable "requests_cpu" {
  type    = string
  default = "500m"
}

variable "requests_memory" {
  type    = string
  default = "512Mi"
}
